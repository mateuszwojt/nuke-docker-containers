# Use a smaller, more efficient base image (if possible)
FROM rockylinux:9 as builder

# Define arguments for Nuke version
ARG NUKE_MAJOR
ARG NUKE_MINOR
ARG NUKE_PATCH
ENV NUKE_VERSION=${NUKE_MAJOR}.${NUKE_MINOR}v${NUKE_PATCH}
ENV NUKE_RELEASE_NAME=Nuke${NUKE_VERSION}

# Environment variables for Nuke installation
ENV NUKE_DIR /usr/local/Nuke${NUKE_VERSION}
ENV NUKE_TEMP_DIR /tmp/Nuke

# Define download URL
ARG NUKE_DOWNLOAD_URL=https://thefoundry.s3.amazonaws.com/products/nuke/releases/${NUKE_VERSION}/${NUKE_RELEASE_NAME}-linux-x86_64.tgz

# Install dependencies, download and extract Nuke, perform cleanup all in one layer
RUN dnf -y install wget && \
    mkdir -p $NUKE_DIR && mkdir -p $NUKE_TEMP_DIR && \
    wget -O /tmp/${NUKE_RELEASE_NAME}-linux-x86_64.tgz ${NUKE_DOWNLOAD_URL} && \
    tar -C ${NUKE_TEMP_DIR} -xvzf /tmp/${NUKE_RELEASE_NAME}-linux-x86_64.tgz && \
    cd ${NUKE_TEMP_DIR} && \
    ./${NUKE_RELEASE_NAME}-linux-x86_64.run --accept-foundry-eula --prefix=/usr/local && \
    dnf clean all && \
    rm -rf /tmp/*

# Remove unnecessary files to optimize the container size
ARG NUKE_FILE_EXCLUDE="Documentation plugins/OCIOConfigs/configs/aces_* plugins/caravr plugins/air libtorch* libcudnn* libcublas* libcusparse* libcusolver* libmkl*"
RUN for pattern in $NUKE_FILE_EXCLUDE; do \
        find $NUKE_DIR -name "$pattern" -exec rm -rf {} +; \
    done

# Use the base image again for the final image
FROM rockylinux:9

# Copy only the necessary files from the builder stage
COPY --from=builder /usr/local /usr/local

# Set environment variables
ENV NUKE_DIR /usr/local/Nuke${NUKE_VERSION}
ENV PATH $PATH:$NUKE_DIR

# Set the default command
LABEL author='mateusz.wojt@outlook.com'
CMD ["nuke", "-t"]